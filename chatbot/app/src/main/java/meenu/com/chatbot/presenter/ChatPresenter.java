package meenu.com.chatbot.presenter;

/**
 * Created by surajit on 27/8/17.
 */

public interface ChatPresenter {
    void sendMessage(String message);
}
